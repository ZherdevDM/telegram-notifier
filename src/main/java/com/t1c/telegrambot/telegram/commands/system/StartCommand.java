package com.t1c.telegrambot.telegram.commands.system;

import com.t1c.telegrambot.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;

import javax.validation.constraints.NotNull;

public class StartCommand extends AbstractSystemCommand {

    private final Logger logger = LoggerFactory.getLogger(StartCommand.class);

    public StartCommand(final String identifier, final String description) {
        super(identifier, description);
    }

    @Override
    public void execute(final AbsSender absSender, final User user, final Chat chat, final String[] strings) {
        @NotNull final String userName = Utils.getUserName(user);

        logger.debug(String.format("Пользователь %s. Начато выполнение команды %s", userName,
                this.getCommandIdentifier()));
        sendAnswer(absSender, chat.getId(), this.getCommandIdentifier(), userName,
                "Давайте начнём! Если Вам нужна помощь, нажмите /help");
        logger.debug(String.format("Пользователь %s. Завершено выполнение команды %s", userName,
                this.getCommandIdentifier()));
    }

}
