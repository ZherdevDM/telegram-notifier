package com.t1c.telegrambot.telegram.commands.operations;

import com.t1c.telegrambot.telegram.commands.system.StartCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;

public class AddReminderCommand extends AbstractOperationCommand {

    private final Logger logger = LoggerFactory.getLogger(StartCommand.class);

    public AddReminderCommand(final String identifier, final String description) {
        super(identifier, description);
    }

    @Override
    public void execute(AbsSender absSender, User user, Chat chat, String[] strings) {

    }

}
